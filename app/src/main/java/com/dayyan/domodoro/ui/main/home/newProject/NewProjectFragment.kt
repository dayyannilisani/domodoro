package com.dayyan.domodoro.ui.main.home.newProject

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.dayyan.domodoro.R
import com.dayyan.domodoro.base.BaseFragment
import com.dayyan.domodoro.base.Stat
import com.dayyan.domodoro.config.MainApplication
import com.dayyan.domodoro.db.models.Project
import com.dayyan.domodoro.ui.main.MainActivityViewModel
import com.dayyan.domodoro.utils.showToast
import kotlinx.android.synthetic.main.fragment_new_project.*
import timber.log.Timber

class NewProjectFragment : BaseFragment() {

    private val viewModel: NewProjectViewModel by viewModels {
        viewModelFactory
    }
    private val activityViewModel: MainActivityViewModel by activityViewModels {
        viewModelFactory
    }
    var priority = 0
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_new_project, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        setOnClickListener()
        setObserver()
    }

    private fun setObserver() {
        viewModel.insertionResult.observe(viewLifecycleOwner, Observer {
            if(it.status == Stat.SUCCESS){
                showToast("Project Insertion was Successful")
                activityViewModel.closeBottomSheet()
                activityViewModel.addProject(Project(name = name_input.text.toString(), description = description_input.text.toString(),priority = priority))
                name_input.setText("")
                description_input.setText("")
                low_pp.isChecked = true
                viewModel.insertionResult.value?.status = Stat.NOTHING
            }else if(it.status == Stat.ERROR){
                if(it.error?.contains("UNIQUE constraint failed",ignoreCase = true)!!){
                    showToast("Please Use a Unique Name")
                }else{
                    showToast("Sorry, Something Went Wrong :((")
                }
                Timber.i(it.error)
            }
        })
    }

    private fun setOnClickListener() {
        create_project.setOnClickListener {
            priority = 0
            when (priorities.checkedRadioButtonId) {
                R.id.low_pp -> priority = 1
                R.id.med_pp -> priority = 2
                R.id.high_pp -> priority = 3
            }
            var isValid = true
            if (name_input.text.isNullOrBlank()) {
                isValid = false
                name_input.error = "Please Enter some name"
            }
            if (description_input.text.isNullOrBlank()) {
                isValid = false
                description_input.error = "Please Enter some name"
            }
            if (isValid) {
                viewModel.insertProject(
                    name = name_input.text.toString(),
                    description = description_input.text.toString(),
                    priority = priority
                )
            }
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (activity?.application as MainApplication).mainComponent().inject(this)
    }
}