package com.dayyan.domodoro.ui.main.home.newTask

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.dayyan.domodoro.R
import com.dayyan.domodoro.base.BaseFragment
import com.dayyan.domodoro.base.Stat
import com.dayyan.domodoro.config.MainApplication
import com.dayyan.domodoro.db.models.Task
import com.dayyan.domodoro.ui.main.MainActivityViewModel
import com.dayyan.domodoro.utils.showToast
import com.google.android.material.datepicker.MaterialDatePicker
import kotlinx.android.synthetic.main.fragment_new_task.*
import timber.log.Timber

class NewTaskFragment : BaseFragment() {

    private val viewModel: NewTaskViewModel by viewModels {
        viewModelFactory
    }
    private val activityViewModel: MainActivityViewModel by activityViewModels {
        viewModelFactory
    }
    var priority = 0
    private lateinit var adapter: ArrayAdapter<String>
    private lateinit var dateBuilder: MaterialDatePicker.Builder<Long>
    private lateinit var materialDatePicker: MaterialDatePicker<Long>
    private var selectedDate: Long = 0
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_new_task, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setAdapter()
        setObservers()
        buildDatePicker()
        setOnClickListeners()
    }

    private fun setAdapter() {
        adapter =
            ArrayAdapter(
                this.requireContext(),
                android.R.layout.simple_spinner_dropdown_item,
                createProjectList()
            )
        project_list.adapter = adapter

    }

    private fun setObservers() {
        activityViewModel.projectsChanged.observe(viewLifecycleOwner, Observer {
            adapter.clear()
            adapter.addAll(createProjectList())
            adapter.notifyDataSetChanged()
        })
        viewModel.insertionResult.observe(viewLifecycleOwner, Observer {
            Timber.i("newTask Id is ${it.response}")
            if(it.status == Stat.SUCCESS){
                activityViewModel.addTask(Task(projectName = project_list.selectedItem.toString(),
                    priority = priority,
                    name = name_input.text.toString(),
                    description = description_input.text.toString(),
                    startTime = selectedDate,
                    isFinished = false,
                    taskId = it.response!!.toInt()
                    ))
                activityViewModel.closeBottomSheet()
                description_input.setText("")
                name_input.setText("")
                low_p.isChecked = true
                buildDatePicker()
                adapter.notifyDataSetChanged()
                showToast("Task Created Successfully")
                viewModel.insertionResult.value?.status = Stat.NOTHING
            }else if(it.status == Stat.ERROR){
                showToast(getString(R.string.sorry))
                Timber.i(it.error)
            }
        })
    }

    private fun buildDatePicker() {
        dateBuilder = MaterialDatePicker.Builder.datePicker()
        dateBuilder.setTitleText("Select a Date for Start of the Task")
        materialDatePicker = dateBuilder.build()
        materialDatePicker.addOnPositiveButtonClickListener {
            selectedDate = it
        }
    }

    private fun setOnClickListeners() {
        date_picker.setOnClickListener {
            materialDatePicker.show(activity?.supportFragmentManager!!, "Date_Picker")
        }
        create_task.setOnClickListener {
            var valid = true
            if (name_input.text?.isBlank()!!) {
                valid = false
                name_input.error = "Please Enter a name"
            }
            if (description_input.text?.isBlank()!!) {
                valid = false
                description_input.error = "Please Enter Some Description"
            }
            if (project_list.selectedItem.toString() == "--Select Your Project--" || project_list.selectedItem.toString()  == "--Please Create a Project First--" ){
                valid = false
                showToast("Please Select a Project")
            }
            if (valid){
                priority = 0
                when (priorities.checkedRadioButtonId) {
                    R.id.low_p -> priority = 1
                    R.id.med_p -> priority = 2
                    R.id.high_p -> priority = 3
                }
                viewModel.createTask(
                    name = name_input.text.toString(),
                    description = description_input.text.toString(),
                    priority = priority,
                    date = selectedDate,
                    projectName = project_list.selectedItem.toString()
                )
            }
        }
    }

    private fun createProjectList(): ArrayList<String> {
        val items = ArrayList<String>()
        if (activityViewModel.projects.size != 0) {
            items.add("--Select Your Project--")
            for (p in activityViewModel.projects) {
                items.add(p.project.name)
            }
        } else {
            items.add("--Please Create a Project First--")
        }
        return items
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (activity?.application as MainApplication).mainComponent().inject(this)
    }
}