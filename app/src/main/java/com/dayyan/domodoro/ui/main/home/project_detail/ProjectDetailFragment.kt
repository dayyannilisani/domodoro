package com.dayyan.domodoro.ui.main.home.project_detail

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.dayyan.domodoro.R
import com.dayyan.domodoro.base.BaseFragment
import com.dayyan.domodoro.base.Stat
import com.dayyan.domodoro.config.MainApplication
import com.dayyan.domodoro.db.models.Task
import com.dayyan.domodoro.ui.main.MainActivityViewModel
import com.dayyan.domodoro.utils.showToast
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.android.synthetic.main.fragment_project_detail.*
import timber.log.Timber


class ProjectDetailFragment : BaseFragment() {


    private val viewModel: ProjectDetailViewModel by viewModels {
        viewModelFactory
    }
    private val activityViewModel: MainActivityViewModel by activityViewModels {
        viewModelFactory
    }
    private lateinit var adapter: ProjectDetailAdapter
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_project_detail, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUi()
        setObservers()
        setAdapter()
    }

    private fun setAdapter() {
        val onDelete:(Task,Int)->Unit = {task,index ->
            activityViewModel.detailProject.tasks.removeAt(index)
            task_count.text = (task_count.text.toString().toInt() - 1).toString()
            adapter.notifyItemRemoved(index)
            viewModel.deleteTask(task)
        }
        val onFinish:(Task,Int)->Unit = {task, index ->
            task.isFinished = !task.isFinished
            viewModel.updateTask(task)
            adapter.notifyItemChanged(index)
        }
        adapter = ProjectDetailAdapter(requireContext(),activityViewModel.detailProject.tasks,finishClicked = onFinish,deleteClicked = onDelete)
        tasks.adapter = adapter
    }

    private fun setObservers() {
        viewModel.changeStatus.observe(viewLifecycleOwner, Observer {
            if (it.status == Stat.SUCCESS) {
                showToast("Status Changed Successfully")
            } else if (it.status == Stat.ERROR) {
                showToast(getString(R.string.sorry))
                Timber.i(it.error)
            }
        })
    }

    private fun initUi() {
        with(activityViewModel.detailProject) {
            project_name.text = this.project.name
            project_description.text = this.project.description
            val context = this@ProjectDetailFragment.requireContext()
            when (this.project.priority) {
                1 -> {
                    priority.text = context.getString(R.string.low)
                    priority.setTextColor(ContextCompat.getColor(context, R.color.black))
                }
                2 -> {
                    priority.text = context.getString(R.string.medium)
                    priority.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary))
                }
                3 -> {
                    priority.text = context.getString(R.string.high)
                    priority.setTextColor(ContextCompat.getColor(context, R.color.colorAccent))
                }
            }
            task_count.text = this.tasks.size.toString()
            finish.setOnClickListener {
                MaterialAlertDialogBuilder(context).apply {
                    this.setTitle("Change Status")
                    val stat =
                        if (activityViewModel.detailProject.project.isFinished) "Ongoing" else "Finished"
                    this.setMessage("Are You Sure You Want to Change Status to $stat")
                    this.setNegativeButton("Cancel") { dialogInterface, _ ->
                        dialogInterface.dismiss()
                    }
                    this.setPositiveButton("Yes") { dialogInterface, _ ->
                        viewModel.changeStatus(
                            !activityViewModel.detailProject.project.isFinished,
                            activityViewModel.detailProject.project.name
                        )
                        activityViewModel.detailProject.project.isFinished =
                            !activityViewModel.detailProject.project.isFinished
                        dialogInterface.dismiss()
                    }
                    this.show()
                }
            }
        }

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (activity?.application as MainApplication).mainComponent().inject(this)
    }
}