package com.dayyan.domodoro.ui.main.home.main

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.dayyan.domodoro.R
import com.dayyan.domodoro.db.models.ProjectWithTasks

class MainAdapter(
    private val context: Context,
    private var projectList: ArrayList<ProjectWithTasks>,
    private val moreClicked: (ProjectWithTasks) -> Unit,
    private val deleteClicked:(ProjectWithTasks) -> Unit
) :
    ListAdapter<ProjectWithTasks, MainAdapter.Holder>(ProjectDiffUtil()) {
    lateinit var view: View
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        view = LayoutInflater.from(parent.context).inflate(R.layout.item_project, parent, false)
        return Holder(view)
    }

    override fun getItemCount(): Int {
        return projectList.count()
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bindView(projectList[position])
    }

    fun updateList(_projectList: ArrayList<ProjectWithTasks>) {
        this.notifyDataSetChanged()
    }

    inner class Holder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {

        private val name = itemView?.findViewById<TextView>(R.id.name)!!
        private val description = itemView?.findViewById<TextView>(R.id.description)!!
        private val priority = itemView?.findViewById<TextView>(R.id.priority)!!
        private val completed = itemView?.findViewById<TextView>(R.id.completed_num)!!
        private val ongoing = itemView?.findViewById<TextView>(R.id.ongoing_num)!!
        private val more = itemView?.findViewById<ImageView>(R.id.more)!!
        private val delete = itemView?.findViewById<ImageView>(R.id.delete)!!
        fun bindView(project: ProjectWithTasks) {

            more.setOnClickListener {
                moreClicked(project)
            }
            delete.setOnClickListener {
                deleteClicked(project)
            }
            var countFin = 0
            for (t in project.tasks) {
                if (t.isFinished)
                    countFin++
            }
            when (project.project.priority) {
                1 -> {
                    priority.text = context.getString(R.string.low)
                    priority.setTextColor(ContextCompat.getColor(context, R.color.black))
                }
                2 -> {
                    priority.text = context.getString(R.string.medium)
                    priority.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary))
                }
                3 -> {
                    priority.text = context.getString(R.string.high)
                    priority.setTextColor(ContextCompat.getColor(context, R.color.colorAccent))
                }
            }
            name.text = project.project.name
            description.text = project.project.description
            completed.text = countFin.toString()
            ongoing.text = (project.tasks.size - countFin).toString()
        }

    }
}
