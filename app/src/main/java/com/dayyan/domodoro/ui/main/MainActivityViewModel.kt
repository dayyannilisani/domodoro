package com.dayyan.domodoro.ui.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.dayyan.domodoro.db.models.Project
import com.dayyan.domodoro.db.models.ProjectWithTasks
import com.dayyan.domodoro.db.models.Task
import com.dayyan.domodoro.di.main.MainScope
import javax.inject.Inject

@MainScope
class MainActivityViewModel @Inject constructor() : ViewModel() {
    val bottomSheetState = MutableLiveData<Boolean>(false)
    val projectsChanged = MutableLiveData<Boolean>(false)
    val projectDeleted = MutableLiveData(-1)
    val projectRetrieved = MutableLiveData(-1)
    val projects = ArrayList<ProjectWithTasks>()
    lateinit var deletedProject:ProjectWithTasks
    var deletedProjectIndex = 0
    lateinit var  detailProject:ProjectWithTasks
    val goToDetail = MutableLiveData<Boolean>(false)

    fun goToDetailProject(project:ProjectWithTasks){
        detailProject = project
        goToDetail.value = true
        goToDetail.value = false
    }
    fun closeBottomSheet() {
        bottomSheetState.value = true
        bottomSheetState.value = false
    }
    fun changeProjects(){
        projectsChanged.value = true
        projectsChanged.value = false
    }
    private fun deleteProjectStatus(){
        projectDeleted.value = deletedProjectIndex
        projectDeleted.value = -1
    }

    private fun retrieveProjectStatus(){
        projectRetrieved.value = deletedProjectIndex
        projectRetrieved.value = -1
    }
    fun setProjectList(projectList:List<ProjectWithTasks>){
        projects.clear()
        projects.addAll(projectList)
        changeProjects()
    }

    fun addProject(project: Project){
        val newProjectTask = ProjectWithTasks(project, mutableListOf())
        projects.add(newProjectTask)
        changeProjects()
    }

    fun addTask(task: Task){
        for (project in projects){
            if(project.project.name ==  task.projectName){
                project.tasks.add(task)
            }
        }
        changeProjects()
    }
    fun deleteProject(name:String){
        for (project in projects){
            if (project.project.name == name){
                deletedProject = project
                deletedProjectIndex = projects.indexOf(project)

            }
        }
        projects.remove(deletedProject)
        deleteProjectStatus()
    }


    fun retrieveProject(){
        projects.add(deletedProjectIndex,deletedProject)
        retrieveProjectStatus()
    }
}