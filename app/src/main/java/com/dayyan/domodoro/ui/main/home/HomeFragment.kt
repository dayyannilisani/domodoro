package com.dayyan.domodoro.ui.main.home

import android.animation.*
import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.view.animation.OvershootInterpolator
import android.view.inputmethod.InputMethodManager
import androidx.activity.OnBackPressedCallback
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.animation.doOnEnd
import androidx.core.content.ContextCompat.getColor
import androidx.core.view.GravityCompat
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import com.dayyan.domodoro.R
import com.dayyan.domodoro.base.BaseFragment
import com.dayyan.domodoro.config.MainApplication
import com.dayyan.domodoro.ui.main.MainActivityViewModel
import com.dayyan.domodoro.ui.main.home.main.MainFragment
import com.dayyan.domodoro.ui.main.home.search.SearchFragment
import com.getkeepsafe.taptargetview.TapTarget
import com.getkeepsafe.taptargetview.TapTargetSequence
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.android.synthetic.main.fragment_bottom_sheet.*
import kotlinx.android.synthetic.main.fragment_home.*
import javax.inject.Inject
import kotlin.math.hypot


class HomeFragment : BaseFragment() {

    @Inject
    lateinit var searchFragment: SearchFragment

    @Inject
    lateinit var mainFragment: MainFragment

    private val viewModel: HomeViewModel by viewModels {
        viewModelFactory
    }
    private val activityViewModel:MainActivityViewModel by activityViewModels{
        viewModelFactory
    }


    private lateinit var bottomSheetBehavior: BottomSheetBehavior<ConstraintLayout>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if(viewModel.isItTheFirstTime())
            tapTarget()
        viewModel.setIsTheFirstTime()
        uiInit()
        setOnClickListeners()
        textChangeListener()
        setOnBackPressed()
        setBottomSheetStateListener()
        setObservers()
    }

    private fun tapTarget() {
        TapTargetSequence(requireActivity()).targets(
            TapTarget.forView(floating, "ADD Item", "first Add a Project, then You can Add Task"),
            TapTarget.forView(more, "More", "It doesn't Nothing, but you can Click it anyway :))"),
            TapTarget.forView(search, "Search", "It search, like that wasn't Obvious"),
            TapTarget.forView(
                menu,
                "Drawer",
                "again it's just for show, but has a little extra to it ;)"
            )
        ).start()
    }

    private fun setObservers() {
        activityViewModel.bottomSheetState.observe(viewLifecycleOwner, Observer {
            if (it) {
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
            }
        })
        activityViewModel.goToDetail.observe(viewLifecycleOwner, Observer {
            if (it) {
                view?.findNavController()
                    ?.navigate(HomeFragmentDirections.actionHomeFragmentToProjectDetailFragment())
            }
        })
    }


    private fun uiInit(){
        navigation.itemIconTintList = null
        bottomSheetBehavior = BottomSheetBehavior.from(bottom)
        bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
        pager_new_item.adapter = NewItemAdapter(this)
        TabLayoutMediator(tab_layout,pager_new_item){ tab, position ->
            if(position == 0) {
                tab.text = getString(R.string.create_project)
            }else{
                tab.text = getString(R.string.create_task)
            }
            pager_new_item.setCurrentItem(tab.position, true)
        }.attach()
        home_frag.adapter = HomeAdapter(this)
        home_frag.isUserInputEnabled = false

    }
    private fun setBottomSheetStateListener() {
        bottomSheetBehavior.addBottomSheetCallback(object :
            BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                    try {
                        floating?.isClickable = true

                        val toLeftXProperty = PropertyValuesHolder.ofFloat(
                            View.X,
                            this@HomeFragment.view?.width!!.toFloat(),
                            viewModel.floatingX - 15f
                        )
                        val toRightXProperty = PropertyValuesHolder.ofFloat(
                            View.X,
                            viewModel.floatingX - 15f,
                            viewModel.floatingX
                        )
                        val toLeft = ObjectAnimator.ofPropertyValuesHolder(floating, toLeftXProperty)
                            .setDuration(200)
                        val toRight = ObjectAnimator.ofPropertyValuesHolder(floating, toRightXProperty)
                            .setDuration(150)
                        val animatorSet = AnimatorSet()
                        animatorSet.play(toLeft).before(toRight)
                        animatorSet.start()
                    }catch (t:Throwable){}
                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {}
        })
    }

    private fun changeFragment() {
        if (viewModel.searching) {
            home_frag.setCurrentItem(1,true)

        } else {
            home_frag.setCurrentItem(0,true)
        }
    }

    private fun setOnBackPressed() {
        val callback: OnBackPressedCallback =
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    if (viewModel.searching) {
                        viewModel.searching = false
                        app_bar.setBackgroundColor(
                            getColor(
                                this@HomeFragment.requireContext(),
                                R.color.colorAccent
                            )
                        )
                        changeFragment()
                        main_tool_bar.visibility = View.VISIBLE
                        search_tool_bar.visibility = View.GONE
                        statusColorChange(1)
                        toggleSearchVisibility(1)
                    } else {
                        if (drawer.isDrawerOpen(GravityCompat.START)) {
                            drawer.closeDrawers()
                        } else {
                            if (bottomSheetBehavior.state == BottomSheetBehavior.STATE_EXPANDED) {
                                bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
                            } else {
                                activity?.finishAndRemoveTask()
                            }
                        }
                    }
                }
            }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)
    }

    private fun textChangeListener() {
        search_input.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {}

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (p0?.length!! > 0) {
                    clear.visibility = View.VISIBLE
                } else {
                    clear.visibility = View.INVISIBLE
                }
            }
        })
    }

    @SuppressLint("RtlHardcoded")
    private fun setOnClickListeners() {
        search.setOnClickListener {
            viewModel.searching = true
            closeBottomSheet()
            floating.animate().alpha(0f).setDuration(200).start()
            changeFragment()
            main_tool_bar.visibility = View.GONE
            search_tool_bar.visibility = View.VISIBLE
            app_bar.setBackgroundColor(getColor(this.requireContext(), R.color.colorPrimary))
            statusColorChange(0)
            toggleSearchVisibility(0)

        }
        back.setOnClickListener {
            viewModel.searching = false
            changeFragment()
            app_bar.setBackgroundColor(getColor(this.requireContext(), R.color.colorAccent))
            main_tool_bar.visibility = View.VISIBLE
            search_tool_bar.visibility = View.GONE
            val imm =
                this.activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(requireView().windowToken, 0)
            statusColorChange(1)
            toggleSearchVisibility(1)
        }
        clear.setOnClickListener {
            search_input.setText("")
        }
        menu.setOnClickListener {
            closeBottomSheet()
            drawer.openDrawer(Gravity.LEFT)
        }
        floating.setOnClickListener {
            floating.isClickable = false
            viewModel.floatingX = floating.x
            val toLeftXProperty = PropertyValuesHolder.ofFloat(
                View.X,
                viewModel.floatingX,
                viewModel.floatingX - 15f
            )
            val toRightXProperty = PropertyValuesHolder.ofFloat(
                View.X,
                viewModel.floatingX - 15f,
                this.view?.width!!.toFloat()
            )
            val toLeft =
                ObjectAnimator.ofPropertyValuesHolder(floating, toLeftXProperty).setDuration(200)
            val toRight =
                ObjectAnimator.ofPropertyValuesHolder(floating, toRightXProperty).setDuration(150)
            val animatorSet = AnimatorSet()
            animatorSet.play(toLeft).before(toRight)
            animatorSet.doOnEnd {
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
            }
            animatorSet.start()
        }
    }

    private fun closeBottomSheet() {
        if (bottomSheetBehavior.state == BottomSheetBehavior.STATE_EXPANDED) {
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
        }
    }

    private fun toggleSearchVisibility(state: Int) {
        if (state == 0) {
            search_tool_bar.apply {
                val cx = width / 8 * 7
                val cy = height / 2
                val finalRadius = hypot(width.toDouble(), height.toDouble()).toFloat()
                val anim = ViewAnimationUtils.createCircularReveal(this, cx, cy, 0f, finalRadius)
                this.visibility = View.VISIBLE
                anim.doOnEnd {
                    search_input.requestFocus()
                    val imm =
                        this@HomeFragment.activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.showSoftInput(search_input, InputMethodManager.SHOW_IMPLICIT)
                    floating.animate().alpha(0f).setDuration(200).start()
                }
                anim.start()
            }
        } else {
            main_tool_bar.apply {
                val cx = width / 8 * 7
                val cy = height / 2
                val finalRadius = hypot(width.toDouble(), height.toDouble()).toFloat()
                val anim = ViewAnimationUtils.createCircularReveal(this, cx, cy, 0f, finalRadius)
                this.visibility = View.VISIBLE
                anim.doOnEnd {
                    floating.animate().apply {
                        interpolator = OvershootInterpolator()
                        duration = 200
                        alpha(1f)
                    }.start()
                }
                anim.start()
            }
        }
    }

    private fun statusColorChange(state: Int) {
        val colorFrom: Int
        val colorTo: Int
        if (state == 0) {
            colorFrom = getColor(this.requireContext(), R.color.colorPrimaryDark)
            colorTo = getColor(this.requireContext(), R.color.colorAccent)
        } else {
            colorFrom = getColor(this.requireContext(), R.color.colorAccent)
            colorTo = getColor(this.requireContext(), R.color.colorPrimaryDark)
        }
        val colorAnimation =
            ValueAnimator.ofObject(ArgbEvaluator(), colorFrom, colorTo)
        colorAnimation.duration = 200
        colorAnimation.addUpdateListener { animator ->
            activity?.window?.statusBarColor = (animator.animatedValue as Int)
        }
        colorAnimation.start()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (activity?.application as MainApplication).mainComponent().inject(this)
    }
}