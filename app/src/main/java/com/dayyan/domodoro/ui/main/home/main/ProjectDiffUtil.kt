package com.dayyan.domodoro.ui.main.home.main

import androidx.recyclerview.widget.DiffUtil
import com.dayyan.domodoro.db.models.ProjectWithTasks

class ProjectDiffUtil:DiffUtil.ItemCallback<ProjectWithTasks>() {
    override fun areItemsTheSame(oldItem: ProjectWithTasks, newItem: ProjectWithTasks): Boolean {
        return oldItem.project.name == newItem.project.name && newItem.tasks.size == oldItem.tasks.size
    }

    override fun areContentsTheSame(oldItem: ProjectWithTasks, newItem: ProjectWithTasks): Boolean {
        return oldItem == newItem
    }
}