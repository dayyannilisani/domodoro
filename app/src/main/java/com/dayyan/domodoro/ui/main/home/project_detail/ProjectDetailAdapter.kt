package com.dayyan.domodoro.ui.main.home.project_detail

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.dayyan.domodoro.R
import com.dayyan.domodoro.db.models.ProjectWithTasks
import com.dayyan.domodoro.db.models.Task

class ProjectDetailAdapter(
    private val context: Context,
    private var tasksList: MutableList<Task>,
    private val finishClicked: (Task,Int) -> Unit,
    private val deleteClicked: (Task,Int) -> Unit
) :
    ListAdapter<Task, ProjectDetailAdapter.Holder>(TaskDiffUtil()) {
    lateinit var view: View
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        view = LayoutInflater.from(parent.context).inflate(R.layout.item_task, parent, false)
        return Holder(view)
    }

    override fun getItemCount(): Int {
        return tasksList.count()
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bindView(tasksList[position],position)
    }

    fun updateList(_projectList: ArrayList<ProjectWithTasks>) {
        this.notifyDataSetChanged()
    }

    inner class Holder(itemView: View?) : RecyclerView.ViewHolder(itemView!!) {

        private val name = itemView?.findViewById<TextView>(R.id.name)!!
        private val description = itemView?.findViewById<TextView>(R.id.description)!!
        private val priority = itemView?.findViewById<TextView>(R.id.priority)!!
        private val status = itemView?.findViewById<TextView>(R.id.status)!!
        private val finish = itemView?.findViewById<ImageView>(R.id.finish)!!
        private val delete = itemView?.findViewById<ImageView>(R.id.delete)!!
        fun bindView(task: Task,position: Int) {

            finish.setOnClickListener {
                finishClicked(task,position)
            }
            delete.setOnClickListener {
                deleteClicked(task,position)
            }
            when (task.priority) {
                1 -> {
                    priority.text = context.getString(R.string.low)
                    priority.setTextColor(ContextCompat.getColor(context, R.color.black))
                }
                2 -> {
                    priority.text = context.getString(R.string.medium)
                    priority.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary))
                }
                3 -> {
                    priority.text = context.getString(R.string.high)
                    priority.setTextColor(ContextCompat.getColor(context, R.color.colorAccent))
                }
            }
            name.text = task.name
            description.text = task.description
            status.text = if (task.isFinished) "Finished" else "Ongoing"
        }

    }
}