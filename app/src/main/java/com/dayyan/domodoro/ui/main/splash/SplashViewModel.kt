package com.dayyan.domodoro.ui.main.splash

import androidx.lifecycle.MutableLiveData
import com.dayyan.domodoro.base.MainBaseViewModel
import com.dayyan.domodoro.base.MySingleObserver
import com.dayyan.domodoro.base.QueryBaseResponse
import com.dayyan.domodoro.db.daos.ProjectDao
import com.dayyan.domodoro.db.models.ProjectWithTasks
import com.dayyan.domodoro.sharedPreferences.UserInfo
import com.dayyan.domodoro.utils.backGround
import io.reactivex.disposables.Disposable
import javax.inject.Inject

class SplashViewModel @Inject constructor(private val userInfo:UserInfo,private val projectDao: ProjectDao):MainBaseViewModel() {
    val state = MutableLiveData(0)
    val projectList = MutableLiveData<QueryBaseResponse<List<ProjectWithTasks>>>()
    private val disposables = ArrayList<Disposable>()
    fun isThisFirstTime(){
        val result = userInfo.getString("FirstTime")
        if(result == "False"){
            getProjectList()
        }else{
            state.value = 1
        }
    }

    private fun getProjectList() =
        projectDao.getUnFinishedProjectsWithTasks().subscribeOn(backGround)
            .observeOn(backGround)
            .subscribe(MySingleObserver(projectList,disposables = disposables))

    override fun onCleared() {
        super.onCleared()
        for(d in disposables)
            d.dispose()
    }
}