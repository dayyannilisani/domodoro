package com.dayyan.domodoro.ui.main.home.project_detail

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.dayyan.domodoro.base.MySingleObserver
import com.dayyan.domodoro.base.QueryBaseResponse
import com.dayyan.domodoro.db.daos.ProjectDao
import com.dayyan.domodoro.db.daos.TaskDao
import com.dayyan.domodoro.db.models.Task
import com.dayyan.domodoro.utils.backGround
import io.reactivex.disposables.Disposable
import javax.inject.Inject

class ProjectDetailViewModel @Inject constructor(private val projectDao: ProjectDao,private val taskDao: TaskDao):ViewModel(){
    val changeStatus = MutableLiveData<QueryBaseResponse<Int>>()
    private val disposables = ArrayList<Disposable>()

    fun changeStatus(finishIt:Boolean,name:String){
        if(finishIt){
            projectDao.finishProject(name).observeOn(backGround)
                .subscribeOn(backGround)
                .subscribe(MySingleObserver(changeStatus,disposables = disposables))
        }else{
            projectDao.unFinishProject(name).observeOn(backGround)
                .subscribeOn(backGround)
                .subscribe(MySingleObserver(changeStatus,disposables = disposables))
        }
    }
    fun deleteTask(task: Task){
        taskDao.delete(task).subscribeOn(backGround).observeOn(backGround)
            .subscribe(MySingleObserver<Int>(disposables = disposables))
    }
    fun updateTask(task:Task){
        taskDao.update(task).subscribeOn(backGround).observeOn(backGround)
            .subscribe(MySingleObserver<Int>(disposables = disposables))
    }

    override fun onCleared() {
        super.onCleared()
        for (d in disposables)
            d.dispose()
    }
}