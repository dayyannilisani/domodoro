package com.dayyan.domodoro.ui.main

import android.os.Bundle
import androidx.activity.viewModels
import com.dayyan.domodoro.R
import com.dayyan.domodoro.base.BaseActivity
import com.dayyan.domodoro.config.MainApplication

class MainActivity : BaseActivity() {


    private val viewModel: MainActivityViewModel by viewModels {
        viewModelFactory
    }

    override fun inject() {
        (applicationContext as MainApplication).mainComponent().inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        inject()
        setContentView(R.layout.activity_main)
    }



}
