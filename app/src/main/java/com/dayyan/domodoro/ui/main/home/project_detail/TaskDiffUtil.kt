package com.dayyan.domodoro.ui.main.home.project_detail

import androidx.recyclerview.widget.DiffUtil
import com.dayyan.domodoro.db.models.ProjectWithTasks
import com.dayyan.domodoro.db.models.Task

class TaskDiffUtil: DiffUtil.ItemCallback<Task>() {
    override fun areItemsTheSame(oldItem: Task, newItem: Task): Boolean {
        return oldItem.taskId == newItem.taskId
    }


    override fun areContentsTheSame(oldItem: Task, newItem: Task): Boolean {
        return oldItem.taskId == newItem.taskId
    }
}