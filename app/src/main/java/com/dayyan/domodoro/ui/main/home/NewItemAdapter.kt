package com.dayyan.domodoro.ui.main.home

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.dayyan.domodoro.ui.main.home.newProject.NewProjectFragment
import com.dayyan.domodoro.ui.main.home.newTask.NewTaskFragment

class NewItemAdapter(fa: Fragment) : FragmentStateAdapter(fa) {

    override fun getItemCount(): Int = 2

    override fun createFragment(position: Int): Fragment = when(position){
            0 ->  NewProjectFragment()
            else ->  NewTaskFragment()
        }
}