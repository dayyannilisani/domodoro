package com.dayyan.domodoro.ui.main.home

import androidx.lifecycle.MutableLiveData
import com.dayyan.domodoro.base.MainBaseViewModel
import com.dayyan.domodoro.di.main.MainScope
import com.dayyan.domodoro.sharedPreferences.UserInfo
import javax.inject.Inject


class HomeViewModel @Inject constructor(private val userInfo: UserInfo):MainBaseViewModel() {
    var searching = false
    var floatingX:Float = 0f

    fun setIsTheFirstTime(){
        userInfo.setString("FirstTime","False")
    }

    fun isItTheFirstTime() = userInfo.getString("FirstTime") != "False"

}