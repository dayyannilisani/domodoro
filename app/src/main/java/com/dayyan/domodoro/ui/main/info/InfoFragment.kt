package com.dayyan.domodoro.ui.main.info

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.OvershootInterpolator
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.viewpager2.widget.ViewPager2
import com.dayyan.domodoro.R
import com.dayyan.domodoro.config.MainApplication
import com.dayyan.domodoro.databinding.FragmentInfoBinding
import kotlinx.android.synthetic.main.fragment_info.*

class InfoFragment : Fragment() {

    private lateinit var binding: FragmentInfoBinding
    private var slideFinished = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_info, container, false)
        val pagerAdapter = PagerAdapter(this)
        binding.pager.adapter = pagerAdapter
        binding.pager.setPageTransformer(PagerTransformer())
        binding.indicator.setViewPager2(binding.pager)
        binding.button.setOnClickListener {
            if (slideFinished){
                view?.findNavController()?.navigate(InfoFragmentDirections.actionInfoFragmentToHomeFragment())
            }else {
                if (binding.pager.currentItem < 2) {
                    if (binding.pager.currentItem == 1) {
                        if (!slideFinished) {
                            changeState()
                        }
                    }
                    binding.pager.setCurrentItem(pager.currentItem + 1, true)
                }
            }
        }
        binding.pager.registerOnPageChangeCallback(object :ViewPager2.OnPageChangeCallback(){
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                if(position == 2 && !slideFinished){
                    changeState()
                }
            }
        })
        return binding.root
    }
    private fun changeState(){
        binding.button.text = getString(R.string.start)
        binding.button.scaleX = 0.9f
        binding.button.scaleY = 0.9f
        binding.button.animate().apply {
            scaleX(1f)
            scaleY(1f)
            duration = 500
            interpolator = OvershootInterpolator()
        }.start()
        slideFinished = true
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (activity?.application as MainApplication).mainComponent().inject(this)
    }
}