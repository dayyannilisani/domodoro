package com.dayyan.domodoro.ui.main.info

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.dayyan.domodoro.R

class PagerAdapter(fa: Fragment) : FragmentStateAdapter(fa) {
    private val slides = listOf(
        R.drawable.ic_tasks to "Create Your Projects and Set Tasks for it",
        R.drawable.ic_notify to "Set Time for Your Tasks and We'll Notify You",
        R.drawable.ic_visual to "See the Results Using Charts"
    )

    override fun getItemCount(): Int = slides.count()

    override fun createFragment(position: Int): Fragment = SliderFragment(slides[position])
}