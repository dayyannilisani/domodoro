package com.dayyan.domodoro.ui.main.home.main

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.dayyan.domodoro.R
import com.dayyan.domodoro.base.BaseFragment
import com.dayyan.domodoro.config.MainApplication
import com.dayyan.domodoro.db.models.ProjectWithTasks
import com.dayyan.domodoro.di.main.MainScope
import com.dayyan.domodoro.ui.main.MainActivityViewModel
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_main.*
import timber.log.Timber
import javax.inject.Inject

@MainScope
class MainFragment @Inject constructor(): BaseFragment() {

    private val activityViewModel: MainActivityViewModel by activityViewModels{
        viewModelFactory
    }

    private val viewModel:MainViewModel by viewModels {
        viewModelFactory
    }
    private lateinit var adapter: MainAdapter
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        createAdapter()
        setObservers()
    }

    private fun setObservers() {
        activityViewModel.projectsChanged.observe(viewLifecycleOwner, Observer {
            if(it){
                adapter.notifyDataSetChanged()
            }
        })
        activityViewModel.projectDeleted.observe(viewLifecycleOwner, Observer {
            if(it >= 0){
                adapter.notifyItemRemoved(it)
            }
        })
        activityViewModel.projectRetrieved.observe(viewLifecycleOwner, Observer {
            if(it>=0){
                adapter.notifyItemInserted(it)
            }
        })
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun createAdapter() {
        val moreClicked : (ProjectWithTasks)->Unit = {
            activityViewModel.goToDetailProject(it)
        }
        val deleteClicked:(ProjectWithTasks)->Unit = {
            activityViewModel.deleteProject(it.project.name)
            val snackBar = Snackbar.make(this.requireView(), getString(R.string.deleted), 2500)
                .setAction(getString(R.string.undo)) {
                        activityViewModel.retrieveProject()
                }
            snackBar.addCallback(object : BaseTransientBottomBar.BaseCallback<Snackbar>() {
                override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
                    if(event != 1){
                        try {
                            viewModel.deleteProject(it.project.name)
                        }catch (t:Throwable){}
                    }
                }
            })
            snackBar.animationMode = Snackbar.ANIMATION_MODE_SLIDE
            snackBar.show()

        }
        adapter = MainAdapter(this.requireContext(),activityViewModel.projects,moreClicked,deleteClicked)
        projects.adapter = adapter
        projects.setOnTouchListener { _, _ ->
            activityViewModel.closeBottomSheet()
            false
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (activity?.application as MainApplication).mainComponent().inject(this)
    }
}