package com.dayyan.domodoro.ui.main.home.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.dayyan.domodoro.base.MySingleObserver
import com.dayyan.domodoro.base.QueryBaseResponse
import com.dayyan.domodoro.base.Stat
import com.dayyan.domodoro.db.daos.ProjectDao
import com.dayyan.domodoro.db.daos.TaskDao
import com.dayyan.domodoro.di.main.MainScope
import com.dayyan.domodoro.utils.backGround
import io.reactivex.SingleObserver
import io.reactivex.disposables.Disposable
import javax.inject.Inject

@MainScope
class MainViewModel @Inject constructor(private val projectDao: ProjectDao,private val taskDao: TaskDao):ViewModel() {

    val disposables = ArrayList<Disposable>()
    val deleteResult = MutableLiveData<QueryBaseResponse<Int>>()
    fun deleteProject(name:String){
        taskDao.deleteProjectTasks(name).subscribeOn(backGround)
            .observeOn(backGround).subscribe(object :SingleObserver<Int>{
                override fun onSuccess(t: Int) {
                    projectDao.deleteProject(name).subscribeOn(backGround).observeOn(backGround).subscribe(MySingleObserver(deleteResult,disposables = disposables))
                }

                override fun onSubscribe(d: Disposable) {
                 disposables.add(d)
                }

                override fun onError(e: Throwable) {
                    val newResult = QueryBaseResponse<Int>(status = Stat.ERROR,error = e.message)
                    deleteResult.postValue(newResult)
                }
            })
    }

    override fun onCleared() {
        super.onCleared()
        for (d in disposables)
            d.dispose()
    }
}