package com.dayyan.domodoro.ui.main.home.search

import androidx.lifecycle.ViewModel
import com.dayyan.domodoro.base.MySingleObserver
import com.dayyan.domodoro.db.daos.TaskDao
import com.dayyan.domodoro.db.models.Task
import com.dayyan.domodoro.utils.backGround
import io.reactivex.disposables.Disposable
import javax.inject.Inject

class SearchViewModel @Inject constructor(private val taskDao: TaskDao):ViewModel() {

    private val disposables = ArrayList<Disposable>()

    fun updateTask(task:Task){
        taskDao.update(task).observeOn(backGround)
            .subscribeOn(backGround)
            .subscribe(MySingleObserver<Int>(disposables = disposables))
    }
    fun deleteTask(task: Task){
        taskDao.delete(task).observeOn(backGround)
            .subscribeOn(backGround)
            .subscribe(MySingleObserver<Int>(disposables = disposables))
    }

    override fun onCleared() {
        super.onCleared()
        for (d in disposables)
            d.dispose()
    }
}