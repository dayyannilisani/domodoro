package com.dayyan.domodoro.ui.main.home.newTask

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.dayyan.domodoro.base.MySingleObserver
import com.dayyan.domodoro.base.QueryBaseResponse
import com.dayyan.domodoro.db.daos.ProjectDao
import com.dayyan.domodoro.db.daos.TaskDao
import com.dayyan.domodoro.db.models.Task
import com.dayyan.domodoro.utils.backGround
import io.reactivex.SingleObserver
import io.reactivex.disposables.Disposable
import timber.log.Timber
import javax.inject.Inject


class NewTaskViewModel @Inject constructor(private val projectDao: ProjectDao, private val taskDao: TaskDao):ViewModel(){

    val disposables = ArrayList<Disposable>()
    val insertionResult = MutableLiveData<QueryBaseResponse<Long>>()

    fun createTask(name:String,description:String,priority:Int,date:Long,projectName:String){
        val newTask = Task(
            name = name, description =  description , priority = priority,
            startTime = date, projectName = projectName
        )
        taskDao.insert(newTask).subscribeOn(backGround)
            .observeOn(backGround)
            .subscribe(MySingleObserver(insertionResult,disposables = disposables))
    }

    override fun onCleared() {
        super.onCleared()
        for (d in disposables)
            d.dispose()
    }
}