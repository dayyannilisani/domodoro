package com.dayyan.domodoro.ui.main.home.newProject

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.dayyan.domodoro.base.MySingleObserver
import com.dayyan.domodoro.base.QueryBaseResponse
import com.dayyan.domodoro.db.daos.ProjectDao
import com.dayyan.domodoro.db.models.Project
import com.dayyan.domodoro.utils.backGround
import io.reactivex.SingleObserver
import io.reactivex.disposables.Disposable
import timber.log.Timber
import javax.inject.Inject

class NewProjectViewModel @Inject constructor(private val projectDao: ProjectDao):ViewModel() {

    val insertionResult = MutableLiveData<QueryBaseResponse<Long>>()
    val disposables = ArrayList<Disposable>()
    fun insertProject(name:String,description:String,priority:Int){
        val newProject = Project(name = name,description = description,priority = priority)
        projectDao.insert(newProject).subscribeOn(backGround)
            .observeOn(backGround)
            .subscribe(MySingleObserver(insertionResult,disposables = disposables))
    }

    override fun onCleared() {
        super.onCleared()
        for (d in disposables){
            d.dispose()
        }
    }
}