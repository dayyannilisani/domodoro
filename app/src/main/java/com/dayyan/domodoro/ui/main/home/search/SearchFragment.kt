package com.dayyan.domodoro.ui.main.home.search

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.KeyEvent
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.OvershootInterpolator
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import com.dayyan.domodoro.R
import com.dayyan.domodoro.base.BaseFragment
import com.dayyan.domodoro.config.MainApplication
import com.dayyan.domodoro.db.models.Task
import com.dayyan.domodoro.ui.main.MainActivityViewModel
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_project_detail.*
import kotlinx.android.synthetic.main.fragment_search.*
import timber.log.Timber
import javax.inject.Inject

class SearchFragment @Inject constructor() : BaseFragment() {


    private val activityViewModel:MainActivityViewModel by activityViewModels {
        viewModelFactory
    }
    private val viewModel:SearchViewModel by viewModels {
        viewModelFactory
    }
    private lateinit var adapter: SearchAdapter
    private var firstTime = true
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setTextSearch()
        settingAdapter()
    }

    private fun settingAdapter() {
        val onDelete:(Task,Int)->Unit = {task, index ->
            var pindex=  0
            with(activityViewModel.projects){
                for (p in this)
                    for (t in p.tasks)
                        if (task.taskId == t.taskId) {
                            pindex = this.indexOf(p)
                        }

            }
            activityViewModel.projects[pindex].tasks.remove(task)
            activityViewModel.changeProjects()
            viewModel.deleteTask(task)
            adapter.deleteItem(index)
        }
        val onFinished:(Task,Int)->Unit = {task, index ->
            task.isFinished = !task.isFinished
            viewModel.updateTask(task)
            adapter.notifyItemChanged(index)
            activityViewModel.changeProjects()
        }
        adapter = SearchAdapter(requireContext(), MutableList(0) { Task() },onFinished,onDelete)
        searched.adapter = adapter
    }

    private fun doSearch(input:String) {
        val result = MutableList(0) {Task()}
        with(activityViewModel.projects){
            for (p in this)
                for (t in p.tasks)
                    if(t.name.contains(input) || t.description.contains(input))
                        result.add(t)
        }
        adapter.setList(result)
        if(result.size == 0){
            empty_placeholder.visibility = View.VISIBLE
            empty_placeholder.animate().apply {
                interpolator = OvershootInterpolator()
                duration = 150
            }.start()
        }else{
            empty_placeholder.visibility = View.GONE
        }
    }

    @SuppressLint("UseRequireInsteadOfGet")
    private fun setTextSearch() {
        parentFragment?.view?.findViewById<EditText>(R.id.search_input)!!.setOnEditorActionListener(object :TextView.OnEditorActionListener{
            override fun onEditorAction(p0: TextView?, p1: Int, p2: KeyEvent?): Boolean {
                if(p1 == EditorInfo.IME_ACTION_SEARCH){
                    showList()
                    doSearch(p0?.text?.toString()!!)
                    return true
                }
                return false
            }
        })
    }

    private fun showList(){
        if(firstTime){
            placeholder.visibility = View.GONE
            searched.visibility = View.VISIBLE
        }

    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        (activity?.application as MainApplication).mainComponent().inject(this)
    }
}