package com.dayyan.domodoro.ui.main.splash

import android.content.Context
import android.os.Bundle
import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import com.dayyan.domodoro.R
import com.dayyan.domodoro.base.BaseFragment
import com.dayyan.domodoro.base.Stat
import com.dayyan.domodoro.config.MainApplication
import com.dayyan.domodoro.databinding.FragmentSplashBinding
import com.dayyan.domodoro.ui.main.MainActivityViewModel
import com.dayyan.domodoro.utils.showToast
import timber.log.Timber

class SplashFragment : BaseFragment() {

    /*
    * We're using DataBinding in this fragment so we don't need to run our code in OnViewCreated Block
    * */
    private val viewModel: SplashViewModel by viewModels {
        viewModelFactory
    }

    private val mainActivityViewModel: MainActivityViewModel by activityViewModels {
        viewModelFactory
    }

    private lateinit var binding: FragmentSplashBinding
    private lateinit var counter: CountDownTimer
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_splash, container, false)
        initial()
        return binding.root
    }

    private fun initial() {
        setObservers()
        viewModel.isThisFirstTime()
    }

    private fun setObservers() {
        viewModel.state.observe(viewLifecycleOwner, Observer {
            if (it == 1) {
                counter = object : CountDownTimer(3000, 1000) {
                    override fun onFinish() {
                        view?.findNavController()
                            ?.navigate(SplashFragmentDirections.actionSplashFragmentToInfoFragment())
                    }

                    override fun onTick(p0: Long) {}
                }
                counter.start()
            }
        })
        viewModel.projectList.observe(viewLifecycleOwner, Observer {
            if (it.status == Stat.SUCCESS) {

                mainActivityViewModel.setProjectList(it.response!!)
                counter = object : CountDownTimer(1000, 1000) {
                    override fun onFinish() {
                        view?.findNavController()
                            ?.navigate(SplashFragmentDirections.actionSplashFragmentToHomeFragment())
                    }

                    override fun onTick(p0: Long) {}
                }
                counter.start()
            } else if (it.status == Stat.ERROR) {
                showToast("Sorry, Something Went Wrong :((")
                Timber.i(it.error)
            }
        })
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (activity?.application as MainApplication).mainComponent().inject(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        counter.cancel()
    }
}