package com.dayyan.domodoro.model

enum class Priority (val p:Int){
    LOW(1),
    MEDIUM(2),
    HIGH(3)
}