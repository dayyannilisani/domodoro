package com.dayyan.domodoro.sharedPreferences

import android.content.Context
import com.dayyan.domodoro.utils.preferenceName

class UserInfo(context: Context) {

    private var userPreference = context.getSharedPreferences(preferenceName,Context.MODE_PRIVATE)

    fun setString(key:String,value:String){
        val editor = userPreference.edit()
        editor.putString(key,value)
        editor.apply()
    }

    fun getString(key: String) = userPreference.getString(key,"")
}