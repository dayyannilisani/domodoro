package com.dayyan.domodoro.config

import androidx.multidex.MultiDexApplication
import com.dayyan.domodoro.BuildConfig
import com.dayyan.domodoro.di.AppComponent
import com.dayyan.domodoro.di.DaggerAppComponent
import com.dayyan.domodoro.di.main.MainComponent
import timber.log.Timber

class MainApplication : MultiDexApplication() {
    private lateinit var appComponent: AppComponent
    private var mainComponent: MainComponent? = null

    private fun initAppComponent() {
       appComponent= DaggerAppComponent.builder().application(this).build()
    }

    fun mainComponent(): MainComponent {
        if (mainComponent == null) {
            mainComponent = appComponent.mainComponent().create()
        }
        return mainComponent as MainComponent
    }

    override fun onCreate() {
        super.onCreate()
        initAppComponent()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}