package com.dayyan.domodoro.utils

import android.app.Activity
import android.util.Log
import android.view.Gravity
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.dayyan.domodoro.BuildConfig
import com.dayyan.domodoro.R

fun logger(tag:String, message:String){
    if(BuildConfig.DEBUG){
        Log.v(tag,message)
    }
}

fun Fragment.showToast(message: String, duration: Int = Toast.LENGTH_LONG) {
    if(defaultToast) {
        Toast.makeText(this.context, message, duration).show()
    }else{
        val inflater = layoutInflater
        val view = inflater.inflate(R.layout.toast,(this.context as Activity).findViewById(R.id.toast_lay))
        val toast = Toast(this.requireContext())
        val toastMessage = view.findViewById<TextView>(R.id.toastMessage)
        toast.view = view
        toastMessage.text = message
        toast.setGravity(Gravity.BOTTOM, 0, 40)
        toast.duration = Toast.LENGTH_LONG
        toast.show()

    }
}



