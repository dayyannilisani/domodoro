package com.dayyan.domodoro.utils

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

const val BaseUrl = "https://api.jambod.site"
      val backGround = Schedulers.io()
      val foreGround = AndroidSchedulers.mainThread()

const val accessTokenSP = "accessToken"
const val refreshTokenSP = "refreshToken"
const val preferenceName = "userInformation"
const val DatabaseName = "domodoro_database"

var defaultToast = false
