package com.dayyan.domodoro.utils

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.Window
import com.dayyan.domodoro.R

class LoadingDialog(context:Context):Dialog(context) {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.loading)
        setCancelable(false)
        setCanceledOnTouchOutside(false)
    }
}