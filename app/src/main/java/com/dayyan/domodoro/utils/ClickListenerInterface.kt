package com.dayyan.domodoro.utils

interface ClickListenerInterfaceClickListenerInterface<in T> {
    fun onClick(data: T)
}