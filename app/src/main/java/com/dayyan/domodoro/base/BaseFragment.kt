package com.dayyan.domodoro.base

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import com.dayyan.domodoro.di.ViewModelFactory
import com.dayyan.domodoro.utils.showToast
import javax.inject.Inject

open class BaseFragment() :
    Fragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    fun handleError(message:String){
        showToast(message)
    }

    fun handleError(error: CustomError){
        showToast(error.userMessage)
    }

}