package com.dayyan.domodoro.base

import androidx.lifecycle.MutableLiveData
import io.reactivex.SingleObserver
import io.reactivex.disposables.Disposable

class MySingleObserver<T>(
    private val response: MutableLiveData<QueryBaseResponse<T>>? = null,
    private val loading: MutableLiveData<Boolean>? = null,
    private val disposables: ArrayList<Disposable>? = null
) : SingleObserver<T> {
    override fun onSuccess(t: T) {
        val result = QueryBaseResponse(response = t, error = null, status = Stat.SUCCESS)
        response?.postValue(result)
        loading?.postValue(false)
    }

    override fun onSubscribe(d: Disposable) {
        disposables?.add(d)
    }

    override fun onError(e: Throwable) {
        val result = QueryBaseResponse<T>(response = null, error = e.message, status = Stat.ERROR)
        response?.postValue(result)
        loading?.postValue(false)
    }
}