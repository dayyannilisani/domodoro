package com.dayyan.domodoro.base

import android.view.View
import androidx.lifecycle.MutableLiveData
import com.dayyan.domodoro.utils.logger
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import org.json.JSONObject
import retrofit2.HttpException


class MyObserver<T>(
    private val apiResponse: MutableLiveData<BaseResponse<T>>,
    private val loadingStatus: MutableLiveData<Int>? = null
) : Observer<T> {
    private val tag = "MyObserver"
    private val tempResponse = BaseResponse<T>()
    private val error = CustomError()
    override fun onComplete() {}

    override fun onSubscribe(d: Disposable) {
        try {
            loadingStatus!!.postValue(View.VISIBLE)
        } catch (t: Throwable) {
        }
        tempResponse.status = Stat.LOADING
        apiResponse.postValue(tempResponse)
    }

    override fun onNext(t: T) {
        try {
            loadingStatus!!.postValue(View.GONE)
        } catch (t: Throwable) {
        }
        logger(tag,"Api call succeed")
        tempResponse.data = t
        tempResponse.status = Stat.SUCCESS
        apiResponse.postValue(tempResponse)
    }

    override fun onError(t: Throwable) {
        extractError(t)
        try {
            loadingStatus!!.postValue(View.GONE)
        } catch (t: Throwable) {
        }
        tempResponse.status = Stat.ERROR
        tempResponse.message = error.userMessage
        apiResponse.postValue(tempResponse)
    }

    private fun extractError(t: Throwable) {
        try {
            val errorBody = t as HttpException
            val jsonError = JSONObject(errorBody.response()!!.errorBody()!!.string())

            val errorJson = jsonError.getJSONObject("error")
            error.code = errorJson.getInt("code")
            error.message = errorJson.getString("message")
            error.userMessage = errorJson.getString("userMessage")
            tempResponse.error = error
        } catch (t: Throwable) {
            error.code = 400
            error.message = "couldn't connect to server"
            error.userMessage = "خطا در ارتباط با سرور"
            tempResponse.error = error
        }
    }

}