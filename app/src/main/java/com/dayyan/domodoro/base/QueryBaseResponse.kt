package com.dayyan.domodoro.base

data class QueryBaseResponse<T>(
    var response:T? = null,
    var error:String? = null,
    var status: Stat
)