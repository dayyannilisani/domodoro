package com.dayyan.domodoro.base

import androidx.lifecycle.ViewModel
import com.dayyan.domodoro.network.MainApi
import javax.inject.Inject

open class MainBaseViewModel @Inject constructor() : ViewModel() {
    @Inject
    lateinit var mainApi: MainApi
}