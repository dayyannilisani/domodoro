package com.dayyan.domodoro.base

enum class Stat{
    SUCCESS,
    ERROR,
    LOADING,
    NOTHING
}