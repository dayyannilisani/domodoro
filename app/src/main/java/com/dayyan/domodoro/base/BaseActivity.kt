package com.dayyan.domodoro.base

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.dayyan.domodoro.di.ViewModelFactory
import com.dayyan.domodoro.utils.LoadingDialog
import javax.inject.Inject

@SuppressLint("Registered")
abstract class BaseActivity : AppCompatActivity() {
    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    lateinit var loadingDialog: LoadingDialog

    abstract fun inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loadingDialog = LoadingDialog(this)
    }

    fun setLoadingStatus(status: Boolean) {
        if (status) {
            loadingDialog.show()
        } else {
            loadingDialog.hide()
        }
    }

}