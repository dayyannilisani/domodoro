package com.dayyan.domodoro.base

class BaseResponse<T> {
    var data:T? = null
    var status:Stat = Stat.NOTHING
    var message:String = ""
    var error:CustomError? = null
}