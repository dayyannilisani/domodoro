package com.dayyan.domodoro.base

data class CustomError(
    var code:Int = 0,
    var message:String = "",
    var userMessage:String = ""
)