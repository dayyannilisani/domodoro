package com.dayyan.domodoro.db.models

import android.os.Parcelable
import androidx.room.Embedded
import androidx.room.Relation
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ProjectWithTasks(
    @Embedded val project: Project,
    @Relation(
        parentColumn = "name",
        entityColumn = "projectName"
    )
    val tasks: MutableList<Task>
):Parcelable