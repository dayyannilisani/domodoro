package com.dayyan.domodoro.db.daos

import androidx.room.*
import com.dayyan.domodoro.db.models.Project
import com.dayyan.domodoro.db.models.ProjectWithTasks
import io.reactivex.Single

@Dao
interface ProjectDao {
    @Insert
    fun insert(project: Project):Single<Long>

    @Update
    fun update(project: Project)

    @Delete
    fun delete(project: Project)

    @Query("select * from projects where isFinished = 0")
    fun getUnFinishedProjectsWithTasks():Single<List<ProjectWithTasks>>

    @Query("select count(*) from projects where isFinished = 0")
    fun getUnfinishedCount():Single<Int>

    @Query("select count(*) from projects where isFinished = 1")
    fun getFinishedCount():Single<Int>

    @Query("delete from projects where name = :name")
    fun deleteProject(name:String):Single<Int>

    @Query("update projects set isFinished = 1 where name = :name")
    fun finishProject(name:String):Single<Int>

    @Query("update projects set isFinished = 0 where name = :name")
    fun unFinishProject(name:String):Single<Int>
}