package com.dayyan.domodoro.db.models

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(
    tableName = "tasks"
)
data class Task(
    @PrimaryKey(autoGenerate = true)
    var taskId: Int = 0,
    var projectName:String = "",
    var name: String = "",
    var priority: Int = 0,
    var domodoros: Int = 0,
    var description: String = "",
    var startTime: Long = 0,
    var isFinished:Boolean = false
):Parcelable