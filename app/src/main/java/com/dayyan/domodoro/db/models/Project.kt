package com.dayyan.domodoro.db.models

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import com.dayyan.domodoro.model.Priority
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "projects", indices = [Index(value = ["name"], unique = true)])
data class Project(
    @PrimaryKey(autoGenerate = false)
    var name:String = "",
    var priority: Int = 0,
    var description:String = "",
    var isFinished:Boolean = false
):Parcelable