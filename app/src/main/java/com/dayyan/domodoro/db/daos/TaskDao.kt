package com.dayyan.domodoro.db.daos

import androidx.room.*
import com.dayyan.domodoro.db.models.Task
import io.reactivex.Single

@Dao
interface TaskDao {
    @Insert
    fun insert(task: Task):Single<Long>

    @Update
    fun update(task: Task):Single<Int>

    @Delete
    fun delete(task: Task):Single<Int>

    @Query("delete from tasks where projectName = :name")
    fun deleteProjectTasks(name:String):Single<Int>
//    @Query("select * from tasks where taskId = :id")
//    fun getOne(id:Int)
}