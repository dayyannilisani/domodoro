package com.dayyan.domodoro.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.dayyan.domodoro.db.daos.ProjectDao
import com.dayyan.domodoro.db.daos.TaskDao
import com.dayyan.domodoro.db.models.Project
import com.dayyan.domodoro.db.models.Task

@Database(
    entities = [Project::class, Task::class],
    version = 1,
    exportSchema = false
)
abstract class DomodoroDatabase : RoomDatabase() {
    abstract val taskDao: TaskDao
    abstract val projectDao: ProjectDao
}