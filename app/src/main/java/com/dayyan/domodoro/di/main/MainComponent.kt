package com.dayyan.domodoro.di.main

import com.dayyan.domodoro.ui.main.MainActivity
import com.dayyan.domodoro.ui.main.home.HomeFragment
import com.dayyan.domodoro.ui.main.home.main.MainFragment
import com.dayyan.domodoro.ui.main.home.newProject.NewProjectFragment
import com.dayyan.domodoro.ui.main.home.newTask.NewTaskFragment
import com.dayyan.domodoro.ui.main.home.project_detail.ProjectDetailFragment
import com.dayyan.domodoro.ui.main.home.search.SearchFragment
import com.dayyan.domodoro.ui.main.info.InfoFragment
import com.dayyan.domodoro.ui.main.info.SliderFragment
import com.dayyan.domodoro.ui.main.splash.SplashFragment
import dagger.Module
import dagger.Subcomponent

@MainScope
@Subcomponent(modules = [MainModule::class,MainViewModelModule::class])
interface MainComponent {
    @Subcomponent.Factory
    interface Factory {
        fun create(): MainComponent
    }

    fun inject(mainActivity: MainActivity)
    fun inject(splashFragment: SplashFragment)
    fun inject(infoFragment: InfoFragment)
    fun inject(homeFragment: HomeFragment)
    fun inject(mainFragment: MainFragment)
    fun inject(searchFragment: SearchFragment)
    fun inject(newTaskFragment: NewTaskFragment)
    fun inject(newProjectFragment: NewProjectFragment)
    fun inject(projectDetailFragment: ProjectDetailFragment)
}