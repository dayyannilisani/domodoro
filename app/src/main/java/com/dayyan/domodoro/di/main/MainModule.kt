package com.dayyan.domodoro.di.main

import com.dayyan.domodoro.db.DomodoroDatabase
import com.dayyan.domodoro.db.daos.ProjectDao
import com.dayyan.domodoro.db.daos.TaskDao
import com.dayyan.domodoro.network.MainApi
import com.dayyan.domodoro.sharedPreferences.UserInfo
import com.dayyan.domodoro.utils.BaseUrl
import com.dayyan.domodoro.utils.accessTokenSP
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.Request
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

@Module
class MainModule {

    @MainScope
    @Provides
    fun provideMainService(userInfo: UserInfo): MainApi {
        return Retrofit.Builder().baseUrl(BaseUrl)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .client(
                OkHttpClient.Builder().addInterceptor {
                    val request: Request =
                        it.request().newBuilder()
                            .addHeader(
                                "Authorization",
                                "Bearer " + userInfo.getString(accessTokenSP)
                            )
                            .build()
                    it.proceed(request)
                }.connectTimeout(10, TimeUnit.SECONDS).build()
            ).build().create(MainApi::class.java)
    }


    @MainScope
    @Provides
    fun provideProjectDao(db:DomodoroDatabase):ProjectDao =
        db.projectDao

    @MainScope
    @Provides
    fun provideTaskDao(db:DomodoroDatabase):TaskDao =
        db.taskDao

}

