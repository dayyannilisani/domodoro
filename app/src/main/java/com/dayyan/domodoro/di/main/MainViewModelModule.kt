package com.dayyan.domodoro.di.main

import androidx.lifecycle.ViewModel
import com.dayyan.domodoro.di.ViewModelKey
import com.dayyan.domodoro.ui.main.MainActivityViewModel
import com.dayyan.domodoro.ui.main.home.HomeViewModel
import com.dayyan.domodoro.ui.main.home.main.MainViewModel
import com.dayyan.domodoro.ui.main.home.newProject.NewProjectViewModel
import com.dayyan.domodoro.ui.main.home.newTask.NewTaskViewModel
import com.dayyan.domodoro.ui.main.home.project_detail.ProjectDetailViewModel
import com.dayyan.domodoro.ui.main.home.search.SearchViewModel
import com.dayyan.domodoro.ui.main.splash.SplashViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class MainViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(MainActivityViewModel::class)
    abstract fun bindMainActivityViewModel(viewModel: MainActivityViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SplashViewModel::class)
    abstract fun bindSplashViewModel(viewModel: SplashViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    abstract fun bindHomeViewModel(viewModel: HomeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(NewProjectViewModel::class)
    abstract fun bindNewProjectViewModel(viewModel: NewProjectViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(NewTaskViewModel::class)
    abstract fun bindNewTaskViewModel(viewModel: NewTaskViewModel):ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindMainViewModel(viewModel: MainViewModel):ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ProjectDetailViewModel::class)
    abstract fun bindProjectDetailViewModel(viewModel: ProjectDetailViewModel):ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(SearchViewModel::class)
    abstract fun bindSearchViewModel(viewModel: SearchViewModel):ViewModel
}