package com.dayyan.domodoro.di

import android.app.Application
import androidx.room.Room
import androidx.room.RoomDatabase
import com.dayyan.domodoro.db.DomodoroDatabase
import com.dayyan.domodoro.sharedPreferences.UserInfo
import com.dayyan.domodoro.utils.DatabaseName
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object AppModule {
    @Singleton
    @Provides
    fun providesUserInfo(application: Application): UserInfo = UserInfo(application)


    @Singleton
    @Provides
    fun provideDomodoroDatabase(application:Application):DomodoroDatabase =
        Room.databaseBuilder(application,DomodoroDatabase::class.java, DatabaseName).build()
}