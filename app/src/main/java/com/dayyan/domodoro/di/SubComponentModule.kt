package com.dayyan.domodoro.di


import com.dayyan.domodoro.di.main.MainComponent
import dagger.Module


@Module(subcomponents = [MainComponent::class])
object SubComponentModule